﻿Feature: Jenkins new project
 
Scenario: Wrong project added
	Given I added a new project called "EmptyTestProject"
	When I delete it using the name "EmptyTestProject" in the URL	
	Then The "EmptyTestProject" project URL should not be available anymore