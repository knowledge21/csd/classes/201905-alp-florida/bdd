﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace DojoCucumber
{
    [Binding]
    public class StepDefinitions
    {
        private IWebDriver driver;
        
        [Given(@"I added a new project called ""(.*)""")]
        public void GivenIAddedANewProjectCalled(string projectName)
        {

            driver = new ChromeDriver();

            driver.Navigate().GoToUrl("http://localhost:8084/");
            driver.FindElement(By.CssSelector("#tasks > div:nth-child(1) > a.task-link")).Click();

            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 5));
            wait.Until(w => driver.FindElement(By.Id("name")).Displayed);

            driver.FindElement(By.Id("name")).SendKeys(projectName);
            driver.FindElement(By.CssSelector("#j-add-item-type-standalone-projects > ul > li.hudson_model_FreeStyleProject")).Click();

            driver.FindElement(By.Id("ok-button")).Click();
            driver.FindElement(By.Id("yui-gen41-button")).Click();

        }

        [When(@"I delete it using the name ""(.*)"" in the URL")]
        public void WhenIDeleteIt(string projectName)
        {
            driver.Navigate().GoToUrl("http://localhost:8084/job/" + projectName);
            driver.FindElement(By.CssSelector("#tasks > div:nth-child(7) > a.task-link")).Click();

            WebDriverWait alertWait = new WebDriverWait(driver, new TimeSpan(3000));
            alertWait.Until(ExpectedConditions.AlertIsPresent());
            IAlert alert = driver.SwitchTo().Alert();
            alert.Accept();
        }

        [Then(@"The ""(.*)"" project URL should not be available anymore")]
        public void ThenItsURLShouldNotBeAvailableAnymore(string projectName)
        {
            driver.Navigate().GoToUrl("http://localhost:8084/job/" + projectName);
            string screenMessage = driver.FindElement(By.CssSelector("body > h2")).Text;

            Assert.AreEqual("HTTP ERROR 404", screenMessage);

            driver.Close();
            driver.Quit();
        }
    }
}
